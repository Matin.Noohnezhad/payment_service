FROM maven:3.8.5-openjdk-18

WORKDIR /tmp/payment_service
ENTRYPOINT ["mvn", "spring-boot:run"]
