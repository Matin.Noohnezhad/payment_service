package com.matin.payment_service.dao;

import java.util.List;

import com.matin.payment_service.model.Wallet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletRepository extends JpaRepository<Wallet, Integer> {

    List<Wallet> findByUserId(String userId);
}
