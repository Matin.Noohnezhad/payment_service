package com.matin.payment_service.services;

import java.util.List;

import com.matin.payment_service.dao.WalletRepository;
import com.matin.payment_service.model.RabbitTransactionMessage;
import com.matin.payment_service.model.RabbitUserCreationMessage;
import com.matin.payment_service.model.Wallet;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConsumer {

    @Autowired
    WalletRepository walletRepository;

    @RabbitListener(queues = "${paymentservice.rabbitmq.user_creation_queue}")
    public void receiveMessage(RabbitUserCreationMessage message) {
        System.out.println("--------------------------------");
        System.out.println(message.toString());
        Wallet userWallet = new Wallet(message.userId(), 500.0);
        walletRepository.save(userWallet);
        System.out.println("--------------------------------");
    }

    @RabbitListener(queues = "${paymentservice.rabbitmq.user_payment_rpc_queue1}")
    public boolean rpcReceiveMessage(RabbitTransactionMessage msg) {
        System.out.println("consumer receive msg = " + msg);
        List<Wallet> wallets = walletRepository.findByUserId(msg.userId());
        System.out.println("user wallet is " + wallets);
        Wallet userWallet = wallets.get(0);
        boolean result;
        if (userWallet.getBalance() > msg.price()) {
            userWallet.setBalance(userWallet.getBalance() - msg.price());
            walletRepository.save(userWallet);
            result = true;
        } else {
            result = false;
        }
        return result;
    }

}
