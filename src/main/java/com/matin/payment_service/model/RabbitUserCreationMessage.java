package com.matin.payment_service.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RabbitUserCreationMessage(@JsonProperty("userId") String userId) implements Serializable {
}

// @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,
// property = "@id", scope = RabbitMessage.class)
// public class RabbitMessage implements Serializable {

// @JsonProperty("messageType")
// private MessageType messageType;
// @JsonProperty("userId")
// private String userId;
// @JsonProperty("price")
// private double price;

// public RabbitMessage(MessageType messageType, String userId) {
// this.messageType = messageType;
// this.userId = userId;
// }

// public RabbitMessage(MessageType messageType, String userId, double price) {
// this.messageType = messageType;
// this.userId = userId;
// this.price = price;
// }

// public MessageType getMessageType() {
// return this.messageType;
// }

// public void setMessageType(MessageType messageType) {
// this.messageType = messageType;
// }

// public String getUserId() {
// return this.userId;
// }

// public void setUserId(String userId) {
// this.userId = userId;
// }

// public double getPrice() {
// return this.price;
// }

// public void setPrice(double price) {
// this.price = price;
// }

// @Override
// public String toString() {
// return "{" +
// " messageType='" + getMessageType() + "'" +
// ", userId='" + getUserId() + "'" +
// ", price='" + getPrice() + "'" +
// "}";
// }

// public enum MessageType {
// USER_CREATION,
// TRANSACTION
// }
// }