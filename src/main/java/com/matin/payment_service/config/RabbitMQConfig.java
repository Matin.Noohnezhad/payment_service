package com.matin.payment_service.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${paymentservice.rabbitmq.exchange}")
    String EXCHANGE_NAME;
    @Value("${paymentservice.rabbitmq.routingkey}")
    String ROUTING_KEY;
    @Value("${paymentservice.rabbitmq.user_creation_queue}")
    String USER_CREATION_QUEUE;
    // RPC configurations
    @Value("${paymentservice.rabbitmq.user_payment_rpc_queue1}")
    String RPC_QUEUE1;
    @Value("${paymentservice.rabbitmq.user_payment_rpc_queue2}")
    String RPC_QUEUE2;
    @Value("${paymentservice.rabbitmq.rpc_exchange}")
    String RPC_EXCHANGE;
    @Value("${paymentservice.rabbitmq.rpc_routingkey1}")
    String RPC_ROUTING_KEY1;
    @Value("${paymentservice.rabbitmq.rpc_routingkey2}")
    String RPC_ROUTING_KEY2;

    @Bean
    public DirectExchange appExchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue oneWayQueue() {
        return new Queue(USER_CREATION_QUEUE, false);
    }

    @Bean
    public Binding declareBindingOneWay() {
        return BindingBuilder.bind(oneWayQueue()).to(appExchange()).with(ROUTING_KEY);
    }

    //

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    //
    @Bean
    Queue rpcMsgQueue() {
        return new Queue(RPC_QUEUE1);
    }

    @Bean
    Queue rpcReplyQueue() {
        return new Queue(RPC_QUEUE2);
    }

    @Bean
    DirectExchange rpcExchange() {
        return new DirectExchange(RPC_EXCHANGE);
    }

    @Bean
    Binding rpcMsgBinding() {
        return BindingBuilder.bind(rpcMsgQueue()).to(rpcExchange()).with(RPC_ROUTING_KEY1);
    }

    @Bean
    Binding rpcReplyBinding() {
        return BindingBuilder.bind(rpcReplyQueue()).to(rpcExchange()).with(RPC_ROUTING_KEY2);
    }

}
